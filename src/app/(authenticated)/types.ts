export type ViewerDocument = {
  title: string;
  description: string;
  chapters: {
    title: Document["title"];
    content: string;
  }[];
  confidential?: boolean;
};
