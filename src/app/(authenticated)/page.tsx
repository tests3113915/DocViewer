import { cookies } from "next/headers";
import { LogOut } from "../_components/Logout";
import { getAllDocumentsByUserAuth } from "../api/documents";
import { DocumentContents } from "../_components/DocumentContents";
import LogIn from "@/app/LogIn/page";
import styles from "./page.module.css";
import "./style.css";

export type AuthorisationLevels = "AAA" | "CommonPeople" | "MiddleMan"; // ? For granular access

export default async function Home() {
  const { value: isLoggedIn } = cookies().get("session") ?? {};

  const documentItem = await getAllDocumentsByUserAuth(Boolean(isLoggedIn));
  return (
    <main className={styles.main}>
      <div style={{ display: "flex", gap: "8rem", height: "100%" }}>
        <DocumentContents documentItem={documentItem} />
        {Boolean(isLoggedIn) ? <LogOut /> : <LogIn />}
      </div>
    </main>
  );
}
