import { DocumentContents } from "@/app/_components/DocumentContents";
import { LogOut } from "@/app/_components/Logout";
import { getAllDocumentsByUserAuth } from "@/app/api/documents";
import { cookies } from "next/headers";
import LogIn from "@/app/LogIn/page";

export default async function ChapterLayout({
  children,
}: {
  children: React.ReactNode;
}) {
  const { value: isLoggedIn } = cookies().get("session") ?? {};

  const documentItem = await getAllDocumentsByUserAuth(!!isLoggedIn);
  return (
    <div style={{ display: "flex", height: "100%", width: "100vw" }}>
      <DocumentContents documentItem={documentItem} />
      <section style={{ padding: "1rem 2rem" }}>{children}</section>
      {Boolean(isLoggedIn) ? <LogOut /> : <LogIn />}
    </div>
  );
}
