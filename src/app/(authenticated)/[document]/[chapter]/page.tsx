import allDocuments from "@/documents.json";
import type { ViewerDocument } from "@/app/(authenticated)/types";

export default async function Page({
  params,
  searchParams,
}: {
  params: { document: string; chapter: string };
  searchParams: { chapter: string };
}) {
  const doc = await import(
    `/src/app/storage/${getFileNameFromId({
      id: params.document,
      chapter: searchParams.chapter,
    })}`
  );
  return (
    <section
      style={{ maxWidth: "50vw" }}
      dangerouslySetInnerHTML={{ __html: doc.default }}
    />
  );
}

const getFileNameFromId = ({ id, chapter }: { id: string; chapter: string }) =>
  (allDocuments as Record<string, ViewerDocument>)[id].chapters[Number(chapter)]
    .content;
