import { revalidatePath } from "next/cache";
import { cookies } from "next/headers";
import { redirect } from "next/navigation";

const Page = () => {
  async function handleLogin(e: FormData) {
    "use server";

    const pw = e.get("pw")!.toString();

    if (!pw) return;
    cookies().set("session", pw, {
      httpOnly: true,
      secure: process.env.NODE_ENV === "production",
      maxAge: 60 * 60 * 24 * 7, // One week
      path: "/",
    });
    revalidatePath("/", "layout");
    return redirect("/");
  }

  return (
    <main>
      <form action={handleLogin}>
        Enter password:
        <input type="password" name="pw" />
        <button type="submit">submit</button>
      </form>
    </main>
  );
};

export default Page;
