import allDocuments from "@/documents.json";
import type { ViewerDocument } from "../(authenticated)/types";

export const getAllDocumentsByUserAuth = async (
  isAuthorised: boolean
): Promise<[string, ViewerDocument][]> => {
  const allDocs = Object.entries(
    allDocuments as Record<string, ViewerDocument>
  );

  return Promise.resolve(
    isAuthorised ? allDocs : allDocs.filter(([, v]) => !v.confidential)
  );
};
