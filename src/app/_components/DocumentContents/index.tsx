import type { ViewerDocument } from "@/app/(authenticated)/types";
import classes from "./index.module.css";
import Link from "next/link";

export const DocumentContents = ({
  documentItem,
}: {
  documentItem: [title: string, value: ViewerDocument][];
}) => (
  <aside className={classes.contents}>
    <h2 className={classes.title}>Table of contents</h2>
    <ul>
      {documentItem.map(([key, value]) => (
        <li key={key}>
          <div>
            <Link
              href={`/${key}/${value.chapters[0].title
                .replace(/\W/g, "")
                .split(" ")
                .join("-")}?chapter=${0}`}
            >
              {value.title}
            </Link>
            <ul>
              {value.chapters.map((chapter, index) => (
                <li key={chapter.content}>
                  <Link
                    style={{
                      padding: "0 1rem",
                      margin: index === 0 ? 0 : "0 2rem",
                    }}
                    href={`/${key}/${value.chapters[index].title
                      .replace(/\W/g, "")
                      .split(" ")
                      .join("-")}?chapter=${index}`}
                  >
                    {chapter.title}
                  </Link>
                </li>
              ))}
            </ul>
          </div>
        </li>
      ))}
    </ul>
  </aside>
);
