import { revalidatePath } from "next/cache";
import { cookies } from "next/headers";
import { redirect } from "next/navigation";

export const LogOut = () => {
  const handleLogOut = async () => {
    "use server";
    cookies().delete("session");
    revalidatePath("/", "layout");
    redirect("/LogIn");
  };

  return (
    <form action={handleLogOut}>
      <button type="submit">Log out</button>
    </form>
  ); // Would add confirmation modal
};
