# A Fake Document Service - mini-project

Hello! This README is to serve as instructions for this mini-project. All required information should be available below and provided as part of this package (if not, please contact us!) Please only spend a **maximum** of a few hours on this. We can discuss your approach as well as any un-implemented features in our second interview stage. To make sure this README is viewed as intended, please use a markdown viewer to view this document.

## Tools/Frameworks to use
Use any fullstack frameworks

**You may** use any bootstrap tools to jumpstart your application (eg. create-next-app) and **you may** use any associated bundler to build/serve your application locally (eg. webpack/vite/etc..). (Please use either **npm or yarn** as a dependency manager but feel free to use basic packages as you see fit). The application **should NOT** use any cloud based services or platforms (The built application should work without making any external network requests other than those to the service itself). The application **should NOT** use any authentication libraries or mechanisms. The application needs only to run locally.

## The task
The task is to build a small frontend application to view and explore some fake documents served as 'chapters' of html content. All metadata about the full documents set has been provided in the documents.json file and the html content of said documents in the 'storage' directory. The user should be able to login and logout (please mock this state and do not implement any real authentication mechanisms) to access the confidential documents.

### The Data

When accessing the provided data (documents.json and html files), please imagine/treat them as both as mock database data and a static data store respectively.

#### documents json
The provided documents.json file is intended as a map of all documents that the application should surface; it provides the structure of the documents, chapters and the relevant html for each chapte as well as if the document requires the accessing user to be 'authenticated'. The schema is described below and is referred to in the above task specification:

```
  <Document ID> : {
    "title" : <Document Title>,
    "description : <Description>,
    "chapters" : [
      {
        "title" : <Chapter Title>,
        "content" : <Link to html content>
      },
      {
        "title" : <Chapter Title>,
        "content" : <Link to html content>
      }
    ],
    "confidential"? : boolean
  }
```

Each 'document' can be referred to by its ```Document ID``` key. Each 'document' contains a title, description and an array of chapters each with a chapter title and a reference to an HTML file containing it's content. The confidential field is optional (if true denotes that the user must prove they are authenticated - again: please don't implement any real authenticaion, this can be mocked in state or similar)

#### Storage html files directory
The storage folder contains all html content referred to in the documents.json data.
For example (from documents.json) the document with Document ID **abc123** contains a chapter with the key **Lorum Ipsum** and the value **lorum_ipsum.html**. This html file exists in the storage directory and contains the html that should be displayed when at the path ```/abc123/Lorum Ipsum```. The html does not need any processing and there are no external resources such as images to worry about!

## Requirements

The application should contain the following pages:

- Search Page
  - A simple search page accessed at path ```/```
  - This page should: Show all available documents from documents.json (that the user has access to) as clickable elements that display both the ```<Document Title>``` and the ```<Description>``` of the document. The clickable link should navigate to the document page a ```/<Document ID>``` (See [documents json](#documents-json) for schema)
  - **NOTE:** Unauthenticated users should not see any confidential documents in search results
- A document page 
  - The document page shows the content of the document seperated by chapter - accessed at path ```/<Document ID>```
  - On the left: there should be a flatlist of all chapter titles (each ```title``` in the ```chapters``` array for that document) which should function as clickable links and should navigate to ```/<Document ID>/<Chapter Title>```.
  - On the right: there should be the html content contained in the refrerence html file for the current chapter.
  - **NOTE:** ```/<Document ID>``` should redirect to show the first chapter in the document's ```chapters``` list. (eg. ```/102310``` should redirect to show the first chapter in the chapters object ```/102310/What are hats```.
- Feel free to add any other pages if you deem it nescessary or useful
- Please include some mechanism on all pages to **mock** login and logout behaviour within the application.


### Example user story

The user loads the application at path ```/``` and is served a list of document titles and descriptions from **documents.json**. On clicking the document with title "**A guide to hats**" they are navigated to the first chapter of the respective document at ```/102310/What are hats``` which displays the list of chapters on the left and the content of the associated chapter on the right from **hats_spec.html**. From here, on the left, they go to the list of chapter titles and click on the second chapter ```Hats and how to use them``` which updated the URL to ```/102310/Hats and how to use them``` and on the right; shows the content of **using_hats.html**.


## What we DO NOT expect:
We do not expect any fancy styling or mobile responsiveness; we are simply looking for a basic featurefull UI that satisfies the requirements in a functional way on a standard 1920x1080 resolution.

## What we expect:
A ZIP of the working application codebase that provides the above described functionality; as well as a text file with a brief bried explanation on how to run the application locally as well as any assumtions made.

## Final bits
If you run into issues with any of the content provided (eg. documents.json); feel free to make your own decision on how to resolve the issue (please mention any assumptions or workarounds made in your attached README). If you deem the content to be broken or unusable for any reason, feel free to fix it; but make sure to note what you had to change!